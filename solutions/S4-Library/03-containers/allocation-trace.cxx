#include <iostream>
#include <string>
#include <list>
#include <set>
#include <unordered_set>
#include <random>
#include "trace-alloc.hxx"

using std::cout;

auto R    = std::random_device{};
auto rect = std::uniform_int_distribution{10, 1000};


template<typename T>
void populate(unsigned N, std::list<T> & container) {
    for (auto k = 1U; k <= N; ++k) container.push_back(rect(R));
}

template<typename T, template<typename U> class Container >
void populate(unsigned N, Container<T> & container) {
    for (auto k = 1U; k <= N; ++k) container.insert(rect(R));
}

template<typename Container>
void print(std::string_view name, Container const& container) {
    cout << name << ": ";
    for (auto k: container) cout << k << " ";
    cout << "\n";
}


template<typename T>
void use_list(unsigned N) {
    cout << "-- std::list<T>:" << __PRETTY_FUNCTION__ << "\n";
    auto mgr     = AllocManager{};
    auto numbers = std::list<T>{};
    populate<T>(N, numbers);
    print("linked-list", numbers);
}

template<typename T>
void use_ordered_set(int N) {
    cout << "-- std::set<T>:" << __PRETTY_FUNCTION__ << "\n";
    auto mgr     = AllocManager{};
    auto numbers = std::set<T>{};
    populate<T>(N, numbers);
    print("tree-set", numbers);
}

template<typename T>
void use_unordered_set(int N) {
    cout << "-- std::set<T>:" << __PRETTY_FUNCTION__ << "\n";
    auto mgr     = AllocManager{};
    auto numbers = std::unordered_set<T>{};
    populate<T>(N, numbers);
    print("hash-set", numbers);
}


int main(int argc, char** argv) {
    auto const N = argc==1 ? 10 : std::stoi(argv[1]);

    use_list<short>(N);
    use_list<int>(N);
    use_list<long>(N);
    use_list<long double>(N);

    use_ordered_set<int>(N);
    use_unordered_set<int>(N);
}
