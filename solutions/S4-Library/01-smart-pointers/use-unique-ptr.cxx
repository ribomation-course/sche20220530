#include <iostream>
#include <memory>
#include "person.hxx"

using std::cout;
using std::endl;
using std::unique_ptr;
using std::make_unique;
using ribomation::Person;
using namespace std::literals::string_literals;


auto func(unique_ptr<Person> q) -> unique_ptr<Person> {
    q->incrAge();
    cout << "[func] q: " << *q << endl;
    return q;
}

void use_unique_ptr() {
    cout << "--- usage of unique_ptr ---\n";
    auto anna = make_unique<Person>("Anna Conda"s, 42);
    cout << "[use_unique_ptr] anna: " << *anna << endl;
    auto ptr = std::move(anna);
    cout << "[use_unique_ptr] ptr: " << *ptr << endl;
    cout << "[use_unique_ptr] anna: " << anna.get() << endl;

    auto p = func(std::move(ptr));
    cout << "[use_unique_ptr] p: " << *p << endl;
    cout << "[use_unique_ptr] anna: " << anna.get() << endl;
    cout << "[use_unique_ptr] ptr: " << ptr.get() << endl;

    ptr = make_unique<Person>("Per Silja", 37);
    cout << "[use_unique_ptr] ptr: " << *ptr << endl;

    p = make_unique<Person>("Sham Poo", 32);
    cout << "[use_unique_ptr] p: " << *p << endl;
}
