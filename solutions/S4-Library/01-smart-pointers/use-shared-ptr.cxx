#include <iostream>
#include <memory>
#include "person.hxx"

using std::cout;
using std::endl;
using std::shared_ptr;
using std::make_shared;
using ribomation::Person;
using namespace std::literals::string_literals;


auto func3(shared_ptr<Person> q) -> shared_ptr<Person> {
    q->incrAge();
    cout << "[func3] q: " << *q << endl;
    cout << "[func3] refcnt: " << q.use_count() << endl;
    return q;
}

auto func2(shared_ptr<Person> q) -> shared_ptr<Person> {
    q->incrAge();
    cout << "[func2] q: " << *q << endl;
    cout << "[func2] refcnt: " << q.use_count() << endl;
    q = func3(q);
    return q;
}

void use_shared_ptr() {
    cout << "--- usage of shared_ptr ---\n";
    auto justin = make_shared<Person>("Justin Time"s, 37);
    cout << "[use_shared_ptr] justin: " << *justin << endl;
    cout << "[use_shared_ptr] refcnt: " << justin.use_count() << endl;
    {
        auto p = func2(justin);
        cout << "[use_shared_ptr] p: " << *p << endl;
        cout << "[use_shared_ptr] refcnt: " << p.use_count() << endl;
    }
    cout << "[use_shared_ptr] justin: " << *justin << endl;
    cout << "[use_shared_ptr] refcnt: " << justin.use_count() << endl;
}
