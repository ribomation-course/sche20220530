#include <iostream>
#include "person.hxx"

using std::cout;
using std::endl;
using ribomation::Person;

extern void use_unique_ptr();
extern void use_shared_ptr();

int main() {
    cout << "# persons: " << Person::getCount() << endl;

    use_unique_ptr();
    cout << "# persons: " << Person::getCount() << endl;

    use_shared_ptr();
    cout << "# persons: " << Person::getCount() << endl;
}

