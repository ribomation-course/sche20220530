#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <string_view>
#include <map>
#include <chrono>
#include <tuple>
#include <stdexcept>
#include <memory>

using namespace std;
namespace chr = std::chrono;
using WordFreqs = map<string_view, unsigned>;

auto load(const string& filename) -> tuple<unique_ptr<char*>, string_view> {
    auto f = ifstream{filename};
    if (!f) throw invalid_argument{"cannot open "s + filename};

    size_t size = f.seekg(0, ios_base::end).tellg();
    f.seekg(0);

    auto buf = new char[size + 1];
    f.read(buf, size);
    buf[size] = '\0';
    return make_tuple(make_unique<char*>(buf), string_view{buf, size});
}

auto aggregate(string_view payload, unsigned min) -> WordFreqs {
    auto       f       = WordFreqs{};
    auto const letters = "abcdefghijklmnopqrstuvwxyz"sv;

    auto start = 0UL;
    do {
        start     = payload.find_first_of(letters, start);
        if (start == string_view::npos) break;

        auto end  = payload.find_first_not_of(letters, start);
        if (end == string_view::npos) break;

        auto word = payload.substr(start, end - start);
        start = end + 1;

        if (word.size() >= min) ++f[word];
    } while (start < payload.size());

    return f;
}

int main() {
    auto startTime = chr::steady_clock::now();
    auto filename    = "../musketeers.txt"s;
    auto minWordSize = 4U;

    auto [res, payload] = load(filename);
    cout << "loaded " << payload.size() << " chars\n";

    auto result      = aggregate(payload, minWordSize);
    for (auto [word, freq]: result)
        cout << setw(12) << word << ": " << freq << endl;

    auto endTime = chr::steady_clock::now();
    auto elapsedTime = chr::duration_cast<chr::milliseconds>(endTime - startTime).count();
    cout << "----\n";
    cout << "elapsed " << elapsedTime << " ms\n";
    cout << "loaded " << payload.size() << " chars\n";
    cout << "parsed " << result.size() << " words\n";
}
