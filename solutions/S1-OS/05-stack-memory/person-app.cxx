#include <iostream>
#include <string>
#include <alloca.h>
using std::cout;
using std::string;
using namespace std::literals::string_literals;

struct Person {
    const string name; 
    unsigned     age;
    Person* next;

    Person(const string& name, unsigned int age, Person* next)
            : name(name), age(age), next(next) {
        cout << "Person(" << name << ", " << age << ", " << next << ") @ " << this << "\n";
    }

    ~Person() {
        cout << "~Person() @ " << this << "\n";
    }
};

void print(Person* link) {
    if (link == nullptr) return;
    cout << "Person{" << link->name << ", " << link->age << "} @ " << link << "\n";
    print(link->next);
}

void dispose(Person* link) {
    if (link == nullptr) return;
    dispose(link->next);
    link->~Person();
}


void run(int n) {
    cout << "-- run(" << n << ") --\n";
    Person* head = nullptr;
    for (; n > 0; --n) {
        auto addr = alloca(sizeof(Person));
        head = new (addr) Person{"nisse-"s + std::to_string(n), 18U + 2 * n, head};
    }
    cout << "----\n";
    print(head);
    cout << "----\n";
    dispose(head);
}

int main() {
    run(5);
    run(10);
}
