#pragma once

#include <string>
#include <stdexcept>
#include <tuple>

#include <cerrno>
#include <cstring>

#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

namespace ribomation {
    using namespace std;

    inline bool exists(const string& filename) {
        struct stat fileInfo{};
        if (stat(filename.c_str(), &fileInfo) == 0) return true;
        if (errno == ENOENT) return false;

        throw runtime_error("stat failed for '"s + filename + "': "s + strerror(errno));
    }

    inline auto map(const string& filename) -> tuple<void*, size_t> {
        int fd = open(filename.c_str(), O_RDWR);
        if (fd < 0) {
            throw invalid_argument{"cannot open '"s + filename + "': " + strerror(errno)};
        }

        struct stat fileInfo{};
        if (fstat(fd, &fileInfo) < 0) {
            throw runtime_error{"fstat failed: "s + strerror(errno)};
        }

        auto segment = (char*) mmap(0, fileInfo.st_size, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, 0);
        if (segment == MAP_FAILED) {
            throw runtime_error("mmap failed: "s + strerror(errno));
        }
        close(fd);

        return {segment, fileInfo.st_size};
    }

    class MemoryMappedFile {
        void* payload;
        size_t payloadSize;

    public:
        explicit MemoryMappedFile(const string& filename)  {
            if (!exists(filename)) throw invalid_argument{filename + " not found"};
            auto t = map(filename);
            payload = get<void*>(t);
            payloadSize = get<size_t>(t);
        }

        virtual ~MemoryMappedFile() {
            munmap(payload, payloadSize);
        }

        [[nodiscard]] char* data() const {
            return reinterpret_cast<char*>(payload);
        }

        [[nodiscard]] size_t size() const {
            return payloadSize;
        }

        [[nodiscard]] char* end() const {
            return data() + size();
        }

        MemoryMappedFile() = delete;
        MemoryMappedFile(const MemoryMappedFile&) = delete;
        MemoryMappedFile& operator=(const MemoryMappedFile&) = delete;
    };

}
