#include <iostream>
#include <stdexcept>
#include <locale>
#include <cstring>
#include <cerrno>
#include <sys/mman.h>

using std::runtime_error;
using std::cout;
using namespace std::literals::string_literals;

struct MemorySegment {
    void* start;
    size_t size;

    explicit MemorySegment(size_t size_) : size{size_} {
        start = mmap(nullptr, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
        if (start == MAP_FAILED) {
            throw runtime_error{"mmap(2) failed: "s + strerror(errno)};
        }
    }

    ~MemorySegment() {
        munmap(start, size);
    }
};

auto gauss_sum(long N) {
    return N * (N + 1) / 2;
}

int main() {
    auto const N      = 1'000'000L;
    auto const size   = N * sizeof(int);
    auto       memory = MemorySegment{size};

    int* array = static_cast<int*>(memory.start);
    for (auto k = 0; k < N; ++k) array[k] = k + 1;

    auto      sum = 0L;
    for (auto k   = 0; k < N; ++k) sum += array[k];
    cout.imbue(std::locale("en_US.utf8"));
    cout << "sum: " << sum << " (" << gauss_sum(N) << ")\n";
}
