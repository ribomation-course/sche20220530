#include <iostream>
using std::cout;

int function(char ch, int i, long l, long double ld) {
    return static_cast<int>(ch * i * l * ld);
}

int main() {
    auto result = function('A', 42, 1'000, 3.1415);
    cout << result << "\n";
}
