#include <iostream>
#include <stdexcept>
#include <cerrno>
#include <cstring>
#include <cstdlib>
#include <sys/resource.h>
using namespace std;

// run it with:
// ltrace -S ./name-of-app
// ltrace -Sc ./name-of-app

int main() {
    auto          max_vm = (1L << 24);
    struct rlimit rlim{};
    rlim.rlim_cur = max_vm;
    rlim.rlim_max = max_vm;
    if (setrlimit(RLIMIT_AS, &rlim) < 0) {
        throw runtime_error{"setrlimit failed: "s + strerror(errno)};
    }

    auto const size  = 1 << 10;
    auto       count = 0;
    for (void* ptr = nullptr; (ptr = malloc(size)) != nullptr; ++count) {
      //  cout << (count + 1) << ") " << ptr << "\n";
    }
    cout << "allocated " << count << " blocks, of size " << size << "\n";
    cout << "total " << (count * size) << " bytes\n";
}



