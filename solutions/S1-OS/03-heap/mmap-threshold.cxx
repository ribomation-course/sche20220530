#include <iostream>
#include <stdexcept>
#include <iterator>
#include <cerrno>
#include <cstring>
#include <malloc.h>

using namespace std;

int main() {
    auto const threshold = 32 * 1024;
    if (mallopt(M_MMAP_THRESHOLD, threshold) < 0) {
        throw invalid_argument{"mallopt failed: "s + strerror(errno)};
    }

    auto below = malloc(threshold - 8);
    auto above = malloc(threshold + 8);
    cout << "&below threshold: " << below << "\n";
    cout << "&above threshold: " << above << "\n";
    cout << "address difference: " << std::distance((char*)below, (char*)above)<< "\n";
}
