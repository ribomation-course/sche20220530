#include <iostream>

using std::cout;

struct Account {
    int   balance;
    float rate;

    Account(int b, float r) : balance{b}, rate{r} {
        cout << "Account{" << balance << "kr, "
             << rate << "%} @ " << this << "\n";
    }

    ~Account() {
        cout << "~Account() @ " << this << "\n";
    }
};

auto global = Account{200, .2};

int main() {
    cout << "[main] enter\n";
    auto local = Account{100, .1};
    auto blk = new char[10'000'000];
    auto ptr = new Account{300, .3};
    delete ptr;
    delete blk;
    cout << "[main] exit\n";
}
