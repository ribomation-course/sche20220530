#include <iostream>
#include <filesystem>
#include "outfile.hxx"

namespace rm = ribomation::io;
namespace fs = std::filesystem;
using namespace std::literals::string_literals;
using std::cout;

void usecase1() {
    auto filename = fs::path{"./dummy-1.txt"s};
    {
        auto f = rm::OutFile{filename};
        f.println("Hello from a tiny C++ object"s);
        f.println("This is the 2nd line"s);
        f.println("Finally, the last line is here"s);
    }
    auto numBytes = fs::file_size(filename);
    cout << "written " << numBytes << " bytes to " << filename << "\n";
}

void usecase2() {
    auto filename = fs::path{"./dummy-2.txt"s};
    {
        auto f = rm::OutFile{filename};
        f << "Using shift operator" << "\n";
        f << "The 2nd line goes here" << "\n";
        f << "The 3rd line goes here" << "\n";
        f << "The last line is here" << "\n";
    }
    cout << "written " << fs::file_size(filename) << " bytes to " << filename << "\n";
}

int main() {
    usecase1();
    usecase2();
}

