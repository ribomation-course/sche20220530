#pragma once

#include <iostream>

struct SimpleData {
    int first  = 7;
    int second = 31;
    int third  = 63;

    SimpleData() = default;

    explicit SimpleData(int value) : first(value), second(value * 2), third(value * 3) {
        std::cout << "SimpleData{" << value << "} @ " << this << "\n";
    }

    ~SimpleData() {
        std::cout << "~SimpleData() @ " << this << "\n";
    }

    friend auto operator<<(std::ostream& os, const SimpleData& d) -> std::ostream& {
        return os << "Data{" << d.first << ", " << d.second << ", " << d.third << "}";
    }
};
