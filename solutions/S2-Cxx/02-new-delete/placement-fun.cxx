#include <iostream>
#include <cstdlib>
using std::cout;

void* operator new(size_t numBytes, int dummy) {
    cout << "new (" << dummy << ") " << numBytes << " bytes\n";
    return ::calloc(1, numBytes);
}

void* operator new(size_t numBytes, int dummy, int dummy2) {
    cout << "new (" << dummy << ", " << dummy2 << ") " << numBytes << " bytes\n";
    return ::calloc(1, numBytes);
}

int main() {
    {
        auto ptr = new(42) int{10};
        cout << "ptr: " << *ptr << " @ " << ptr << "\n";
        delete ptr;
    }
    cout << "----\n";
    {
        auto ptr = new(10, 20) int{100};
        cout << "ptr: " << *ptr << " @ " << ptr << "\n";
        delete ptr;
    }
}
