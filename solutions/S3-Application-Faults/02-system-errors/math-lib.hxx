#pragma once

#include <stdexcept>
#include <string>

namespace ribomation {
    using std::runtime_error;
    using std::string;

    struct MathError : runtime_error {
        MathError() : runtime_error{"math error"} {}
        explicit MathError(const string& msg) : runtime_error{msg} {}
    };

    struct InfiniteException : MathError {};
    struct NaNException : MathError {};

    auto log_2(double x) -> double;
}
