#include <iostream>
#include "math-lib.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

void invoke(double x) {
    try {
        auto result = log_2(x);
        cout << "log2(" << x << ") = " << result << endl;
    } catch (const InfiniteException& err) {
        cout << "Infinite ERROR: " << err.what() << endl;
    } catch (const NaNException& err) {
        cout << "NaN ERROR: " << err.what() << endl;
    } catch (const exception& err) {
        cout << "ERROR: " << err.what() << endl;
    }
}

int main() {
    invoke(1024);
    invoke(0);
    invoke(-17);
}
