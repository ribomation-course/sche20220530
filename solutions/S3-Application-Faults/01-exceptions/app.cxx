#include <iostream>
#include <string>
#include <stdexcept>
#include <utility>

using std::cout;
using std::string;
using namespace std::literals::string_literals;

struct Trace {
    const string name;
    explicit Trace(string n) : name{std::move(n)} {
        cout << "[" << name << "] enter\n";
    }
    ~Trace() {
        cout << "[" << name << "] exit\n";
    }
};

void bizop() {
    auto t   = Trace{"bizop"s};
    auto txt = "abc"s;
    txt.at(42);
}

void fn2() {
    auto t = Trace{"fn2"s};
    bizop();
}

void fn1() {
    auto t = Trace{"fn1"s};
    fn2();
}

int main() {
    auto t1 = Trace{"main"s};
    try {
        auto t2 = Trace{"try"s};
        fn1();
    } catch (std::exception const& x) {
        cout << "ERR: " << x.what() << "\n";
    }
}
