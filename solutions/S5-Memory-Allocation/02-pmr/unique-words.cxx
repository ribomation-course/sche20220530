#include <iostream>
#include <string>
#include <set>
#include <memory_resource>
#include <cstdint>
#include "no-heap.hxx"

using std::data;
using std::size;
using std::begin;
using std::end;

using std::pmr::string;
using std::pmr::set;

using std::pmr::null_memory_resource;
using std::pmr::monotonic_buffer_resource;
using std::pmr::unsynchronized_pool_resource;
using std::pmr::set_default_resource;


int main() {
    std::uint8_t storage[100'000]{};
    auto upstream = null_memory_resource();
    auto buffer   = monotonic_buffer_resource{data(storage), size(storage), upstream};
    auto memory   = unsynchronized_pool_resource{&buffer};

    set_default_resource(&memory);
    {
        auto        words = set<string>{};
        for (string word; std::cin >> word;) {
            words.insert(std::move(word));
        }
        for (auto const& w: words) std::cout << w << "\n";
    }

    ensure_no_heap();
}
