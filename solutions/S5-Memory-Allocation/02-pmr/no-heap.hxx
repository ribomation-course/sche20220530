#pragma once
#include <cstddef>
#include <new>

auto operator new(size_t numBytes) -> void* {
    throw std::bad_alloc{};
}

auto operator new[](size_t numBytes) -> void* {
    throw std::bad_alloc{};
}

void ensure_no_heap() {
    try {
        new int{42};
        std::printf("unexpected!!\n");
    } catch (std::bad_alloc& x) {
        std::printf("*** Confirmed!\nNo heap space was harmed in this production.\n");
    }
}
