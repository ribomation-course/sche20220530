#include <iostream>
#include <random>
#include "allocators.hxx"

using std::cout;
using namespace ribomation::memory;

auto R      = std::random_device{};
auto number = std::uniform_int_distribution<long>{1, 1'000'000};

void use_mono_allocator(MonotonicAllocator& mem) {
    cout << "-- Using Mono Allocator --\n";
    int repeat = 2;
    while (repeat-- > 0) {
        auto n = 0;
        try {
            while (true) {
                auto value = number(R);
                auto addr  = mem.allocate<long>();
                auto ptr   = new(addr) long{value};
                cout << "ptr: " << *ptr << " @ " << ptr << "\n";
                ++n;
            }
        } catch (std::bad_alloc& x) {
            cout << "allocated " << n << " blocks, before overflow\n";
            mem.reset();
        }
    }
}

void use_circ_allocator(CircularAllocator& mem, int n) {
    cout << "-- Using Circ Allocator --\n";
    for (int k = 0; k < n; ++k) {
        auto ptr   = mem.create<long>(number(R));
        cout << "ptr: " << *ptr << " @ " << ptr << "\n";
    }
}


int main() {
    char buf[32];
    {
        auto mem = MonotonicAllocator{buf, sizeof(buf)};
        use_mono_allocator(mem);
    }
    {
        auto mem = CircularAllocator{buf, sizeof(buf)};
        use_circ_allocator(mem, 3 * sizeof(buf) / sizeof(long));
    }
}
