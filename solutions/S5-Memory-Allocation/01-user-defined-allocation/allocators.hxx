#pragma once
#include <stdexcept>
#include <cstddef>

namespace ribomation::memory {
    using byte = unsigned char;

    struct AbstractAllocator {
        AbstractAllocator(void* storage_, size_t capacity_) :
                storage{storage_},
                storageEnd{storage_ + capacity_},
                nextAddress{reinterpret_cast<byte*>(storage_)} {}

        virtual ~AbstractAllocator() = default;

        void reset() {
            nextAddress = static_cast<byte*>(storage);
        }

        byte* allocate(size_t numBytes) {
            if (willBeOverflow(numBytes)) {
                handleOverflow();
            }
            auto addr = nextAddress;
            nextAddress += numBytes;
            return addr;
        }
        
        template<typename Type>
        Type* allocate() {
            return reinterpret_cast<Type*>(allocate(sizeof(Type)));
        }

        template<typename Type, typename... ConstructorArgs>
        Type* create(ConstructorArgs&& ...args) {
            auto addr = allocate<Type>();
            return new (addr) Type{std::forward<ConstructorArgs...>(args...)}; 
        }

    protected:
        void* storage;
        void* storageEnd;
        byte* nextAddress;
        virtual void handleOverflow() = 0;

        bool willBeOverflow(size_t numBytes) {
            return (nextAddress + numBytes) > storageEnd;
        }
    };

    struct MonotonicAllocator : AbstractAllocator {
        MonotonicAllocator(void* storage, size_t capacity)
                : AbstractAllocator{storage, capacity} {}

        void handleOverflow() override {
            throw std::bad_alloc{};
        }
    };

    struct CircularAllocator : AbstractAllocator {
        CircularAllocator(void* storage, size_t capacity)
                : AbstractAllocator{storage, capacity} {}

        void handleOverflow() override {
            reset();
        }
    };
}

