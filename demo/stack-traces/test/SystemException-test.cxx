#include "catch2-unit-testing.hxx"
#include "stack-trace.hxx"
#include <sstream>
#include <fcntl.h>

using namespace std::literals;
using namespace ribomation;


void noSuchFile() {
    auto filename = "--no-such-file.1234567890.txt"s;
    auto fd       = open(filename.c_str(), O_RDONLY);
    if (fd == -1) {
        throw SystemException{"cannot open '"s + filename + "'"s};
    }
}

void invoke_noSuchFile() {
    noSuchFile();
}

TEST_CASE("throwing a SystemException{}, should be populated by errno", "[app]") {
    try {
        invoke_noSuchFile();
    } catch (const SystemException& err) {
        auto what = string{err.what()};
        REQUIRE_THAT(what, Catch::Contains("no-such-file"s));
        REQUIRE_THAT(what, Catch::Contains("file or directory"s));
        REQUIRE_THAT(what, Catch::Contains("errno=2"s));

        auto message = err.systemError.message();
        REQUIRE_THAT(message, Catch::Equals("No such file or directory"s));

        auto buf = std::ostringstream{};
        err.printStackTrace(buf);

        auto expected = "1) noSuchFile()\n"s +
                        "2) invoke_noSuchFile()\n"s;
        REQUIRE_THAT(buf.str(), Catch::Contains(expected));
    }
}


