#include <iostream>
#include <memory>
#include <cstdlib>
#include "person.hxx"
#include "trace.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation::persons;
using namespace ribomation::util;

struct FreeDeleter {
    void operator()(void* addr) {
        auto t = Trace{"free deleter"};
        t.out() << "disposing " << addr << endl;
        free(addr);
    }
};

template<typename T>
using LegacyPtr = unique_ptr<T, FreeDeleter>;

struct Data {
    int arg;
    int res;
};

auto legacy_func() -> LegacyPtr<Data> {
    auto t = Trace{"legacy_func"};
    auto addr = malloc(sizeof(Data));
    t.out() << "allocated " << addr << endl;
    auto obj = static_cast<Data*>(addr);
    obj->arg = 10;
    obj->res = 55;
    return LegacyPtr<Data>{obj};
}

int main() {
    auto t = Trace{"main"};
    {
        auto b = Trace{"block"};
        auto p = legacy_func();
        b.out() << "p: fib(" << p->arg << ") = " << p->res << endl;
    }
    return 0;
}

