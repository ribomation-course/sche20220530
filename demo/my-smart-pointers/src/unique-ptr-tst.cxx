#include <iostream>
#include <memory>
#include <vector>
#include "person.hxx"
#include "trace.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation::persons;
using namespace ribomation::util;

auto func(unique_ptr<Person> q) -> unique_ptr<Person> {
    auto t = Trace{"func"};
    q->incrAge();
    t.out() << "q: " << q->toString() << endl;

    q = make_unique<Person>("Per Silja", 57);
    t.out() << "q: " << q->toString() << endl;

    return q;
}

int main(int argc, char** argv) {
    auto t = Trace{"main"};

    {
        auto b = Trace{"block-1"};
        auto p = make_unique<Person>("Justin Time", 55);
        (*p).incrAge();
        b.out() << "p: " << p->toString() << endl;

        auto q = unique_ptr<Person>{};
        b.out() << "q: " << (q ? q->toString() : "<null>") << endl;

        b.out() << "q = p\n";
        q = move(p);
        b.out() << "p: " << (p ? p->toString() : "<null>") << endl;
        b.out() << "q: " << (q ? q->toString() : "<null>") << endl;
    }

    {
        auto b = Trace{"block-2"};
        auto p = make_unique<Person>("Anna Conda", 42);
        auto q = func(move(p));
        b.out() << "p: " << (p ? p->toString() : "<null>") << endl;
        b.out() << "q: " << (q ? q->toString() : "<null>") << endl;
    }

    {
        auto b = Trace{"block-3"};
        auto vec = vector<unique_ptr<Person>>{};
        vec.push_back(make_unique<Person>("Anna", 25));
        vec.push_back(make_unique<Person>("Berit", 35));
        vec.push_back(make_unique<Person>("Carin", 45));
        for (auto& p : vec) b.out() << p->toString() << endl;
    }

    return 0;
}
