#include <iostream>
#include <memory>
#include "person.hxx"
#include "trace.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation::persons;
using namespace ribomation::util;


auto func2(shared_ptr<Person> q) -> shared_ptr<Person> {
    auto t = Trace{"func-2"};
    q->incrAge();
    t.out() << "q: " << q->toString() << " (cnt=" << q.use_count() << ")" << endl;
    return q;
}

auto func1(shared_ptr<Person> q) -> shared_ptr<Person> {
    auto t = Trace{"func-1"};
    q->incrAge();
    t.out() << "q: " << q->toString() << " (cnt=" << q.use_count() << ")" << endl;
    return func2(q);
}

int main(int argc, char** argv) {
    auto t = Trace{"main"};
    {
        auto b   = Trace{"block"};
        auto ptr = make_shared<Person>("Nisse Hult", 63);
        b.out() << "ptr: " << ptr->toString() << " (cnt=" << ptr.use_count() << ")" << endl;
        ptr = func1(ptr);
        b.out() << "ptr: " << ptr->toString() << " (cnt=" << ptr.use_count() << ")" << endl;
    }
    return 0;
}


