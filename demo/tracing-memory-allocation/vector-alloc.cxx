#include <iostream>
#include <vector>
#include "trace-alloc.hxx"
using namespace std;

int main() {
    {
        cout << "-- vector<int>{} --\n";
        auto mgr = AllocManager{};
        auto       v = vector<int>{};
        auto const N = 100;
        for (auto  k = 0; k < N; ++k) v.push_back(k + 1);
    }

    {
        cout << "-- vector<int>{}; reserve(100) --\n";
        auto mgr = AllocManager{};
        auto       v = vector<int>{};
        auto const N = 100;
        v.reserve(N);
        for (auto  k = 0; k < N; ++k) v.push_back(k + 1);
    }

}
