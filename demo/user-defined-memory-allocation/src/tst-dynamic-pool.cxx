#include <vector>
#include <cstdio> //printf
#include <alloca.h>
#include "dynamic-block-pool.hxx"
#include "data.hxx"

using namespace std;
using namespace ribomation::memory;
using namespace ribomation::data;

int main(int argc, char** argv) {
    auto N       = 5U;
    auto vec     = vector<Data*>{};
    auto SIZE    = static_cast<unsigned>(N * sizeof(Data));
    auto pool    = DynamicBlockPool<Data>{alloca(SIZE), SIZE};

    for (auto k = 0; !pool.full(); ++k) {
        auto ptr = new (pool.allocate()) Data{k + 1, 3.1415 * (k + 1)};
        vec.push_back(ptr);
    }
    for (auto ptr : vec) {
        printf("* Data(%d, %f)\n", ptr->ival, ptr->dval);
    }
    for (auto ptr : vec) {
        ptr->~Data();
        pool.deallocate(ptr);
    }
    if (pool.available() != N) {
        throw invalid_argument{"allocation mismatch"};
    }
    return 0;
}
