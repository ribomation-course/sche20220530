#pragma once

#include <cstdio> //printf

namespace ribomation::data {

    struct Data {
        int    ival;
        double dval;

        Data(int ival, double dval) : ival(ival), dval(dval) {
            printf("Data{%d, %lf} @ %p\n", ival, dval, this);
        }
        ~Data() {
            printf("~Data() @ %p\n", this);
        }
    };

}


