#include <memory>
#include <stdexcept>
#include <cstdio> //printf
#include "no-heap.hxx"
#include "data.hxx"
#include "static-block-pool.hxx"
#include "smart-pointer-support.hxx"

using std::unique_ptr;
using std::runtime_error;
using ribomation::data::Data;
using ribomation::memory::StaticBlockPool;
using ribomation::memory::PoolDeleter;
using ribomation::memory::UniquePtr;
using ribomation::memory::make_unique;

constexpr auto N = 5U;
using Ptr = UniquePtr<Data, N>;

Ptr func(Ptr ptr) {
    ptr->ival *= 10;
    printf("[func] Data(%d, %lf)\n", ptr->ival, ptr->dval);
    return ptr;
}

int main(int argc, char** argv) {
    auto pool = StaticBlockPool<Data, N>{};
    {
        auto ptr     = make_unique(pool, 10, 5.25);
        printf("[blk] ptr: Data(%d, %lf)\n", ptr->ival, ptr->dval);

        auto ptr2 = func(move(ptr));
        printf("[blk] ptr : %s\n", (ptr.operator bool() ? "<ptr>" : "<null>"));
        printf("[blk] ptr2: Data(%d, %lf)\n", ptr2->ival, ptr2->dval);
        printf("[blk] pool.size = %d\n", pool.allocated());
    }
    printf("[main] pool.size = %d\n", pool.allocated());
    if (pool.allocated() != 0) {
        throw runtime_error{"pool.size != 0"};
    }
    return 0;
}

