cmake_minimum_required(VERSION 3.16)
project(user_defined_memory_allocation)

set(CMAKE_CXX_STANDARD 17)
#set(NOWARN -Wno-unused-parameter -Wno-unused-variable -Wno-unused-but-set-variable -Wno-pointer-arith)
set(WARN -Wall -Wextra -Wfatal-errors )


add_executable(app
        lib/no-heap.hxx
        lib/static-block-pool.hxx
        src/data.hxx
        src/app.cxx
        )
target_compile_options(app PRIVATE ${WARN})
target_include_directories(app PRIVATE lib src)

