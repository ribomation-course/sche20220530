#pragma once

#include <string_view>

namespace ribomation::threads {
    using std::string_view;

    struct Receivable {
        virtual void send(unsigned id, string_view data) = 0;
    };

}