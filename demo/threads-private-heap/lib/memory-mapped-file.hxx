#pragma once

#include <string>
#include <string_view>
#include <stdexcept>
#include <filesystem>
#include <cerrno>
#include <cstring>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/types.h>

namespace ribomation::files {
    namespace fs = std::filesystem;
    using namespace std::string_literals;
    using std::string;
    using std::string_view;
    using std::invalid_argument;

    struct MemoryMappedFile {
        MemoryMappedFile(const string& filename) {
            auto file = fs::path{filename};
            if (fs::is_regular_file(file)) {
                size = fs::file_size(file);
                payload = load(file, size);
            } else {
                throw invalid_argument{"cannot load: "s + filename};
            }
        }

        ~MemoryMappedFile() {
            munmap(payload, size);
        }

        auto data() const -> string_view {
            return {payload, size};
        }

        MemoryMappedFile() = delete;
        MemoryMappedFile(const MemoryMappedFile&) = delete;
        auto operator =(const MemoryMappedFile&) -> MemoryMappedFile& = delete;

    private:
        char* payload;
        unsigned size;

        char* load(fs::path file, unsigned fileSize) {
            auto fd = open(file.c_str(), O_RDWR);
            if (fd == -1) throw invalid_argument{"cannot open "s + file.string()};

            auto content = mmap(0, fileSize, PROT_READ, MAP_PRIVATE, fd, 0);
            if (content == MAP_FAILED) throw invalid_argument{"cannot do mmap: "s + strerror(errno)};
            close(fd);

            return reinterpret_cast<char*>(content);
        }
    };

}
