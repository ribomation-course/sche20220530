#pragma once
#include <mutex>
#include <condition_variable>
#include <array>

namespace ribomation::threads {
    using std::array;
    using std::mutex;
    using std::condition_variable;
    using std::unique_lock;

    template<typename Type, unsigned CAPACITY = 32>
    struct MessageQueue {

        void put(Type x) {
            auto guard = unique_lock<mutex>{entry};
            notFull.wait(guard, [this]{ return count < CAPACITY; });

            queue[putIdx++] = x;
            if (putIdx >= CAPACITY) putIdx = 0;
            ++count;

            notEmpty.notify_all();
        }

        Type get() {
            auto guard = unique_lock<mutex>{entry};
            notEmpty.wait(guard, [this]{ return count > 0; });

            Type x = queue[getIdx++];
            if (getIdx >= CAPACITY) getIdx = 0;
            --count;

            notFull.notify_all();
            return x;
        }

    private:
        array<Type, CAPACITY> queue;
        mutex                 entry;
        condition_variable    notEmpty;
        condition_variable    notFull;

        unsigned putIdx = 0;
        unsigned getIdx = 0;
        unsigned count  = 0;
    };
}

