#include <string>
#include <algorithm>
#include <cstdio>

#include "memory-mapped-file.hxx"
#include "file-loader-thread.hxx"
#include "stripper-thread.hxx"
#include "printer-thread.hxx"
#include "aggregator-thread.hxx"

using namespace std::literals;
using std::string;
using std::stoi;
using std::for_each;

using ribomation::files::MemoryMappedFile;
using ribomation::threads::Receivable;
using ribomation::threads::FileLoader;
using ribomation::threads::Stripper;
using ribomation::threads::Aggregator;
using ribomation::threads::Printer;


int main(int argc, char** argv) {
    auto filename  = "../musketeers.txt"s;
    auto minLength = 4U;
    auto maxWords  = 10U;

    for (auto k=1; k<argc; ++k) {
        string arg = argv[k];
        if (arg == "-f") {
            filename = argv[++k];
        } else if (arg == "-w") {
            minLength = stoi(argv[++k]);
        } else if (arg == "-n") {
            maxWords = stoi(argv[++k]);
        }
    }

    auto content = MemoryMappedFile{filename};
    printf("loaded %ld bytes\n", content.data().size());

    auto printer    = Printer{};
    auto aggregator = Aggregator{maxWords, printer};
    auto stripper   = Stripper{minLength, aggregator};
    auto loader     = FileLoader{content, stripper};

    printer.start();
    aggregator.start();
    stripper.start();
    loader.start();

    loader.join();
    stripper.join();
    aggregator.join();
    printer.join();

    return 0;
}
