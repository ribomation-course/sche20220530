#pragma once

#include <string_view>
#include <iterator>
#include <memory_resource>
#include <thread>
#include <future>
#include <cstdio>

#include "message-queue.hxx"
#include "message.hxx"
#include "receivable.hxx"


namespace ribomation::threads {
    using std::data;
    using std::size;
    using std::thread;
    using std::promise;
    using std::future;

    using std::pmr::monotonic_buffer_resource;
    using std::pmr::unsynchronized_pool_resource;
    using std::pmr::null_memory_resource;
    using std::pmr::new_delete_resource;

    using ribomation::threads::Receivable;
    using ribomation::threads::MessageQueue;
    using ribomation::threads::Message;


    struct Printer : Receivable {
        using MESSAGE = Message<100>;

        void start() {
            thread kicker{&Printer::body, this};
            runner.swap(kicker);
            fullyStarted.get_future().wait();
        }

        void join() { runner.join(); }

        void send(unsigned id, string_view data) {
            auto addr = heap->allocate(sizeof(MESSAGE));
            auto msg  = new(addr) MESSAGE{id, data};
            inbox.put(msg);
        }

    private:
        thread                     runner;
        MessageQueue<MESSAGE*, 16> inbox;
        unsynchronized_pool_resource* heap;
        promise<bool> fullyStarted;

        void body() {
            char storage[100'000];
            auto buffer = monotonic_buffer_resource{data(storage), size(storage), new_delete_resource()};
            auto pool   = unsynchronized_pool_resource{&buffer};
            heap = &pool;
            fullyStarted.set_value(true);
            run();
        }

        void run() {
            do {
                auto msg = inbox.get();

                auto id = msg->getId();

                char payload[MESSAGE::MAX_SIZE];
                msg->copyTo(payload, sizeof(payload));

                msg->~MESSAGE();
                heap->deallocate(msg, sizeof(MESSAGE));
                if (id == 0) break;

                printf("[printer] %s\n", payload);
            } while (true);
        }
    };

}

